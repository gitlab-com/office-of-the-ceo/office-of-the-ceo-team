# Visiting Grant FY23-Q3 Exception Form

Please fill out this form if you want an exception to the $1000 travel allowance that has been allocated as part of the FY23-Q3 Visiting Grant Program.

For details around exceptions, please refer to: https://about.gitlab.com/handbook/incentives/#spending-caps

# {+Requestor Section+}

## Step 1: Personal and trip information

1. Title this issue: `F23-Q3 Visiting Grant Exception: your_name`

Please share the following. 

1. `Team member's full name`: `Job Title, GitLab Handle, Slack Handle and/or email address`
1. Team members home location: `Enter home location (including city and country)`
1. Why do you need an exception to the $1000 travel budget?: `Clarify why the $1,000 is insufficient for enabling you to attend any GitLab FY23-Q4 visiting grant events.`
1. How much in addition to the $1000 travel budget do you require?: `Ask for a dollar (USD) amount that you will need in addition to the $1,000.`
1. Acknowledgement that any amount spent in excess of the amount approved in this issue will be reimbursed to GitLab or paid out of pocket as outlined in the handbook: `Write Confirmed`

## Step 2: Assign to Manager for approval

Team members should mark as confidential and assign to their manager for approval. 

## Step 3: Managers to do

# {+Managers section+}

Managers should confirm that the team member qualifies for a FY23-Q3 budget exception. Details are on what qualifies for an exception over $1,000 is in the handbook: https://about.gitlab.com/handbook/incentives/#spending-caps. 

Please add the following.

1. `Managers full name`: `Job Title, GitLab Handle, Slack Handle and/or email address`
1. Managers approval: `Confirmed or Denied. If denied, please go back to the team member to explain why and close the issue.`
1. Additional spending cap granted in excess of $1,000 USD: `$ USD`
1. Assign to @epetoskey for confirmation and program coordination.

# {+Program coordination section+}

## Step 4: Coordinator to do
1. Evaluate the request. 
1. Write `Approved` or `Denied` in the comments section. Tag both the the requestor and the manager in the note. 
1. Close issue.

---
---
### Do Not Edit Below

