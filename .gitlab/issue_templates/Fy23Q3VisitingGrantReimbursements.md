# Visiting Grant FY23-Q3 TripActions Overage Reimbursements

Please fill out this form if you plan to spend over $1,000 with TripActions or have spent beyond your allocated budget in Expensify. 

# {+Requestor Section+}

## Step 1: Personal and trip information

1. Title this issue: `F23-Q3 Visiting Grant Reimbursements: your_name`

Please share the following. 

1. `Team member's full name`: `Job Title, GitLab Handle, Slack Handle and/or email address`
1. How much over the $1,000 baseline TripActions budget did you spend: `Dollar (USD) amount`
1. How much did you overspend within Expensify: `Dollar (USD) amount`
1. Confirmation that you understand your responsibility to reimburse GitLab as [captured in the handbook](https://about.gitlab.com/handbook/incentives/#additional-spending-guidelines) **by 2022-10-31**: `Confirmed`

## Step 2: Assign to Manager 

Team members should mark as confidential and assign to their manager for acknowledgement. 

## Step 3: Managers to do

# {+Managers section+}

Please add the following.

1. `Managers full name`: `Job Title, GitLab Handle, Slack Handle and/or email address`
1. Managers acknowledgement: `Manager writes, "confirmed" to note that they are aware of their team members obligation to reimburse GitLab for the amount spent beyond their travel cap`
1. Assign to @epetoskey **and** the requestor for program coordination

# {+Requestor Section+}

## Step 4: Team member confirmation of expensing
1. Team member confirms that the team member has reimbursed GitLab through screenshotting evidence of repayment and sharing in comments. The team member tags the program coordinator (@epetoskey) in comments. 

# {+Program coordination section+}

## Step 5: Coordinator to do
1. Close issue to confirm receipt. 

---
---
### Do Not Edit Below
